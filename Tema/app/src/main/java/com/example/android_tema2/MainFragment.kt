package com.example.android_tema2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android_tema2.Models.User
import com.example.tema2.Adapters.UsersListAdapter
import kotlinx.android.synthetic.main.fragment_main.*


class MainFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUsersList()

    }

    private fun setupUsersList() {
        val layoutManager = LinearLayoutManager(context)
        val usersList = ArrayList<User>()
        usersList.add(User("Anghel","Lorena"))
        usersList.add(User("Bedreag","Ioana"))
        usersList.add(User("Costache","Andrei"))

        val usersAdapter = UsersListAdapter(usersList)

        rv_users_list.layoutManager = layoutManager
        rv_users_list.adapter = usersAdapter
    }


}
