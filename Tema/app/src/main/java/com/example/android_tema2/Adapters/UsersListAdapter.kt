package com.example.tema2.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.android_tema2.Models.User
import com.example.android_tema2.R
import kotlinx.android.synthetic.main.item_user.view.*

class UsersListAdapter(
    private val usersList : ArrayList<User>
): RecyclerView.Adapter<UsersListAdapter.UserViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_user,parent,false)

        return UserViewHolder(view)

    }

    override fun getItemCount(): Int = usersList.size

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val user = usersList[position]
        holder.bind(user)
    }

    inner class UserViewHolder(private val view: View) : RecyclerView.ViewHolder(view){

        fun bind(user : User){
            view.tv_first_name.text = user.firstName
            view.tv_last_name.text = user.lastName
        }

    }
}