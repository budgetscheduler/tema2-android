package com.example.tema2.Daos

import androidx.room.Insert
import androidx.room.Query
import com.example.android_tema2.Models.User

interface UserDao {
    @get:Query("SELECT * FROM user")
    val allUsers: List<User?>?

    @Insert
    fun insertAll(vararg users: User?)
}