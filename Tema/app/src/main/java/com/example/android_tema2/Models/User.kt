package com.example.android_tema2.Models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
class User(
    @field:ColumnInfo(name = "first_name") var firstName: String, @field:ColumnInfo(
        name = "last_name"
    ) var lastName: String
) {
    @PrimaryKey(autoGenerate = true)
    var id = 0

}